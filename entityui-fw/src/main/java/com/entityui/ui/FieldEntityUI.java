package com.entityui.ui;

import java.util.List;

import com.entityui.entity.Entity;
import com.entityui.entity.Field;
import com.entityui.entity.FieldEntity;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.ComboBox;

public class FieldEntityUI extends FieldUI {

	private List<? extends Entity> entities;

	public FieldEntityUI(FieldEntity<?> fld, String caption, IFieldUIOwner owner, List<? extends Entity> entities) {
		super(fld, caption, owner);
		this.entities = entities;
	}

	@Override
	protected AbstractField createVaadinField() {
		ComboBox field = new ComboBox(getCaption());
		field.setImmediate(true);
		field.setRequired(!getFld().isNullable());
		field.addValueChangeListener(new ValueChangeListener() {

			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				getOwner().fieldChanged(getFld().getId(), parseValue(value));
			}
		});
		Field lookupField = ((FieldEntity<?>) getFld()).getLookupField();
		for (Entity entity : this.entities) {
			Object caption = entity.getField(lookupField);
			field.addItem(entity);
			field.setItemCaption(entity, String.valueOf(caption));
		}
		return field;
	}

	@Override
	public void setValue(Object value) {
		ComboBox vaadinFld = (ComboBox) getVaadinField();
		Entity entity = parseValue(value);
		vaadinFld.setValue(entity);
	}

	@Override
	public Object getValue() {
		ComboBox vaadinFld = (ComboBox) getVaadinField();
		Object value = vaadinFld.getValue();
		Entity entity = parseValue(value);
		return entity;
	}

	private Entity parseValue(Object value) {
		Entity entity = null;
		if (value != null) {
			entity = (Entity) value;
		}
		return entity;
	}

	// @Override
	// public void setValue(Object value) {
	// LongField vaadinFld = (LongField) getVaadinField();
	// Long longValue = parseValue(value);
	// String stringValue = null;
	// if (longValue != null) {
	// stringValue = String.valueOf(longValue);
	// }
	// vaadinFld.setValue(stringValue);
	// }
	//
	// @Override
	// public Object getValue() {
	// LongField vaadinFld = (LongField) getVaadinField();
	// Object value = vaadinFld.getValue();
	// Long longValue = parseValue(value);
	// return longValue;
	// }
	//
	// private Long parseValue(Object value) {
	// Long longValue = null;
	// if (value != null) {
	// longValue = Long.parseLong("" + value);
	// }
	// return longValue;
	// }

}
