package com.entityui.ui;

import com.entityui.entity.Field;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.TextField;

public class FieldTextUI extends FieldUI {

	private String regex;

	public FieldTextUI(Field fld, String caption, IFieldUIOwner owner, String regex) {
		super(fld, caption, owner);
		this.regex = regex;
	}

	@Override
	protected AbstractField createVaadinField() {
		com.entityui.ui.vaadin.TextField editFld = new com.entityui.ui.vaadin.TextField(getCaption(), this.regex);
		editFld.setImmediate(true);
		editFld.setNullRepresentation("");
		editFld.setNullSettingAllowed(true);
		editFld.setRequired(!getFld().isNullable());
		editFld.addValueChangeListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = -2490511611758325512L;

			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				String stringValue = parseValue(value);
				getOwner().fieldChanged(getFld().getId(), stringValue);
			}
		});

		return editFld;
	}

	@Override
	public void setValue(Object value) {
		TextField vaadinFld = (TextField) getVaadinField();
		String stringValue = parseValue(value);
		vaadinFld.setValue(stringValue);
	}

	@Override
	public Object getValue() {
		TextField vaadinFld = (TextField) getVaadinField();
		return vaadinFld.getValue();
	}

	private String parseValue(Object value) {
		String stringValue = null;
		if (value != null) {
			stringValue = String.valueOf(value);
		}
		return stringValue;
	}

}
