package com.entityui.ui;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.entityui.entity.PersistenceManager;
import com.entityui.entity.Restriction;
import com.entityui.entity.Select;
import com.entityui.entity.User;
import com.entityui.entity.util.SecurityUtil;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.UserError;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class LoginView extends CustomComponent implements View, ClickListener {

	private static final long serialVersionUID = -6026379035512198412L;

	public static final String NAME = "";

	private final TextField userFld;

	private final PasswordField password;

	private final ErrorLabel errorLabel;

	private final Button loginButton;

	private final static Logger logger = LogManager.getLogger(LoginView.class);

	public LoginView() {

		// Create the user input field
		this.userFld = new TextField("User:");
		this.userFld.setWidth("300px");
		this.userFld.setRequired(true);
		this.userFld.setRequiredError("Username is required");
		this.userFld.setValidationVisible(false);
		this.userFld.setNullRepresentation("");

		// Create the password input field
		this.password = new PasswordField("Password:");
		this.password.setWidth("300px");
		this.password.setRequired(true);
		this.password.setRequiredError("Password is required");
		this.password.setValidationVisible(false);
		this.password.setNullRepresentation("");

		// Create login button
		this.loginButton = new Button("Login", this);
		this.loginButton.setClickShortcut(KeyCode.ENTER);

		this.errorLabel = new ErrorLabel();
		this.errorLabel.addStyleName("loginErrorMsg");

		setSizeFull();

		// Add both to a panel
		VerticalLayout fields = new VerticalLayout(this.userFld, this.password, this.errorLabel, this.loginButton);
		fields.setSpacing(true);
		fields.addStyleName("loginForm");
		fields.setSizeUndefined();

		// The view root layout
		VerticalLayout viewLayout = new VerticalLayout();
		viewLayout.addStyleName("loginView");
		viewLayout.addComponent(fields);
		viewLayout.setSizeFull();
		viewLayout.setSpacing(true);
		viewLayout.setMargin(true);
		viewLayout.setComponentAlignment(fields, Alignment.TOP_CENTER);
		viewLayout.setExpandRatio(fields, 1);

		// viewLayout.setStyleName(Reindeer.LAYOUT_BLUE);
		setCompositionRoot(viewLayout);
	}

	public void enter(ViewChangeEvent event) {
		// focus the username field when user arrives to the login view
		this.userFld.focus();
	}

	private boolean validateField(AbstractField fld) {
		try {
			fld.validate();
			fld.setComponentError(null);
			return true;
		} catch (Exception e) {
			fld.setComponentError(new UserError(e.getMessage()));
			// logger.error(e.getMessage());
			return false;
		}
	}

	private boolean validateFields() {

		boolean result = true;

		if (!validateField(this.userFld)) {
			result = false;
		}

		if (!validateField(this.password)) {
			result = false;
		}
		return result;

	}

	// @Override
	public void buttonClick(ClickEvent event) {
		this.errorLabel.clearError();
		if (!validateFields()) {
			return;
		}

		String username = this.userFld.getValue();
		String password = this.password.getValue();

		Select select = new Select(User.class);
		select.addRestriction(Restriction.eq(User.FIELD_USERNAME, username));
		List<User> users = (List<User>) PersistenceManager.getInstance().executeSelect(select);
		if (users != null && !users.isEmpty()) {
			User user = users.get(0);

			String userPass = (String) user.getField(User.FIELD_PASSWORD);
			String inputPass = null;
			try {
				inputPass = SecurityUtil.doHash(password);
			} catch (NoSuchAlgorithmException e) {
				logger.error("Can't process login", e);
				invalidLogin("Can't process your request!");
			}

			if (userPass.equals(inputPass)) {
				// Store the current user in the service session
				getSession().setAttribute(VaadinUI.SESSION_ATTRIBUTE_USER, user);

				// Navigate to main view
				getUI().getNavigator().navigateTo(HomeView.NAME);
			} else {
				invalidLogin("Username or password is incorrect...");
			}
		} else {
			invalidLogin("Username is incorrect");
		}

	}

	private void invalidLogin(String error) {
		// Wrong password clear the password field and refocuses it
		this.password.setValue(null);
		this.password.focus();
		this.errorLabel.setError(error);
	}
}
