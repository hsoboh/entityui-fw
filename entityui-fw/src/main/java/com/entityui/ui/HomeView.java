package com.entityui.ui;

import com.entityui.entity.Entity;
import com.entityui.entity.EntitySchema;
import com.entityui.entity.PersistenceManager;
import com.entityui.entity.User;
import com.entityui.ui.manager.EntityConfig;
import com.entityui.ui.manager.EntityUIManager;
import com.entityui.ui.manager.EntityUnit;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

public class HomeView extends CustomComponent implements View {

	private static final long serialVersionUID = 6462276880441105802L;

	public static final String NAME = "Home";

	private Label label;
	private Button btnLogout;
	private TabSheet tabSheet;

	public HomeView() {
		initComponents();
	}

	private void initComponents() {
		this.label = new Label();
		this.btnLogout = new Button("Logout");
		// this.btnLogout.setIcon(new ThemeResource("img/logout.png"));
		this.btnLogout.addClickListener(new Button.ClickListener() {
			private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
				// "Logout" the user
				getSession().setAttribute(VaadinUI.SESSION_ATTRIBUTE_USER, null);

				// Refresh this view, should redirect to login view
				getUI().getNavigator().navigateTo(LoginView.NAME);
			}
		});

		this.tabSheet = new TabSheet();

	}

	private void initUI() {
		setSizeFull();

		VerticalLayout rootLayout = new VerticalLayout();
		rootLayout.setSizeFull();
		rootLayout.setSpacing(true);
		rootLayout.setMargin(true);

		HorizontalLayout welcomeLayout = new HorizontalLayout();

		welcomeLayout.addComponent(this.label);
		welcomeLayout.addComponent(this.btnLogout);
		welcomeLayout.setWidth("100%");

		welcomeLayout.setComponentAlignment(this.btnLogout, Alignment.TOP_RIGHT);
		rootLayout.addComponent(welcomeLayout);

		MenuBar menubar = new MenuBar();
		menubar.setWidth("100%");
		menubar.setHeight("35px");
		buildMenu(menubar);

		rootLayout.addComponent(menubar);

		this.tabSheet.setSizeFull();

		rootLayout.addComponent(this.tabSheet);
		rootLayout.setExpandRatio(this.tabSheet, 1.0f);

		setCompositionRoot(rootLayout);
	}

	private void buildMenu(MenuBar menubar) {
		EntityUIManager controller = EntityUIManager.getInstance();
		EntityConfig config = controller.getEntityConfig();

		for (final EntityUnit unit : config.getUnits()) {
			String caption = unit.getEntity().getSimpleName();
			menubar.addItem(caption, new Command() {
				private static final long serialVersionUID = -3066552463738073053L;

				public void menuSelected(MenuItem selectedItem) {
					openEntityTab(unit);

				}
			});
		}
	}

	private void openEntityTab(EntityUnit unit) {
		EntitySchema schema = PersistenceManager.getEntitySchema(unit.getEntity());

		Entity listEntity = schema.newEntity();

		EntityListPanel panel = new EntityListPanel(listEntity);
		listEntity.setListView(panel);
		panel.init();

		this.tabSheet.addTab(panel, listEntity.getCaption());
		this.tabSheet.getTab(panel).setClosable(true);
		this.tabSheet.setSelectedTab(panel);
	}

	// private void openTab(String caption, Component tab, String iconName) {
	// ThemeResource image = null;
	// if (iconName != null) {
	// image = new ThemeResource("img/" + iconName);
	// }
	// this.tabSheet.addTab(tab, caption, image);
	// this.tabSheet.getTab(tab).setClosable(true);
	// this.tabSheet.setSelectedTab(tab);
	// }


	// private void buildSettingsMenuItem(MenuBar menubar) {
	// User currentUser = getCurrentUser();
	//
	// MenuBar.MenuItem settings = menubar.addItem("Settings", new
	// ThemeResource("img/settings.png"), null);
	// if (currentUser.isSystemUser() || currentUser.isAdminUser()) {
	// settings.addItem("Users", new ThemeResource("img/users.png"), new
	// Command() {
	//
	// private static final long serialVersionUID = -7461219817863191334L;
	//
	// @Override
	// public void menuSelected(MenuItem selectedItem) {
	// openUsersTab();
	// }
	// });
	// }
	// settings.addItem("Change password", new
	// ThemeResource("img/change-password.png"), new Command() {
	//
	// private static final long serialVersionUID = -5756715025127342920L;
	//
	// @Override
	// public void menuSelected(MenuItem selectedItem) {
	// changePasswordClicked();
	// }
	// });
	// }
	//
	// protected void changePasswordClicked() {
	// ChangePasswordWindow window = new ChangePasswordWindow(getCurrentUser());
	// UI.getCurrent().addWindow(window);
	// }

	// private void openTab(String caption, Component tab, String iconName) {
	// ThemeResource image = null;
	// if (iconName != null) {
	// image = new ThemeResource("img/" + iconName);
	// }
	// this.tabSheet.addTab(tab, caption, image);
	// this.tabSheet.getTab(tab).setClosable(true);
	// this.tabSheet.setSelectedTab(tab);
	// }

	// private void openSubscriptionsGrowthReport() {
	// UI.getCurrent().addWindow(new SubscribersGrowthReport());
	// }

	private User getCurrentUser() {
		return (User) getSession().getAttribute(VaadinUI.SESSION_ATTRIBUTE_USER);
	}

	public void enter(ViewChangeEvent event) {
		User user = getCurrentUser();
		this.label.setValue("Welcome " + user.getField(User.FIELD_USERNAME));

		initUI();// this is because session is still null in constructor
	}

}
