package com.entityui.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

@Theme("entityui-theme")
public class VaadinUI extends UI {

	private static final long serialVersionUID = 1708591003470030918L;

	public static final String SESSION_ATTRIBUTE_USER = "session.user";

	@Override
	protected void init(VaadinRequest request) {
		// Set error handler on session scope
		VaadinSession.getCurrent().setErrorHandler(new GlobalErrorHandler());

		//
		// Create a new instance of the navigator. The navigator will attach
		// itself automatically to this view.
		//
		new Navigator(this, this);

		//
		// The initial log view where the user can login to the application
		//
		getNavigator().addView(LoginView.NAME, LoginView.class);

		//
		// Add the main view of the application
		//
		getNavigator().addView(HomeView.NAME, HomeView.class);

		//
		// We use a view change handler to ensure the user is always redirected
		// to the login view if the user is not logged in.
		//
		getNavigator().addViewChangeListener(new ViewChangeListener() {

			private static final long serialVersionUID = 1L;

			public boolean beforeViewChange(ViewChangeEvent event) {

				// Check if a user has logged in
				boolean isLoggedIn = getSession().getAttribute(SESSION_ATTRIBUTE_USER) != null;
				boolean isLoginView = event.getNewView() instanceof LoginView;

				if (!isLoggedIn && !isLoginView) {
					// Redirect to login view always if a user has not yet
					// logged in
					getNavigator().navigateTo(LoginView.NAME);
					return false;

				} else if (isLoggedIn && isLoginView) {
					// If someone tries to access to login view while logged in,
					// then cancel
					return false;
				}

				return true;
			}

			public void afterViewChange(ViewChangeEvent event) {

			}
		});
	}

}
