package com.entityui.ui;

import java.math.BigDecimal;

import com.entityui.entity.Field;
import com.entityui.ui.vaadin.DecimalField;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.AbstractField;

public class FieldDecimalUI extends FieldUI {

	private int precision;
	private int scale;

	public FieldDecimalUI(Field fld, String caption, IFieldUIOwner owner, int precision, int scale) {
		super(fld, caption, owner);

		this.precision = precision;
		this.scale = scale;
	}

	@Override
	protected AbstractField createVaadinField() {
		DecimalField editFld = new DecimalField(getCaption(), this.precision, this.scale);
		editFld.setImmediate(true);
		editFld.setNullRepresentation("");
		editFld.setNullSettingAllowed(true);
		editFld.setRequired(!getFld().isNullable());
		editFld.addValueChangeListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = -2490511611758325512L;

			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				getOwner().fieldChanged(getFld().getId(), parseValue(value));
			}
		});
		return editFld;
	}

	@Override
	public void setValue(Object value) {
		DecimalField vaadinFld = (DecimalField) getVaadinField();
		BigDecimal decimalValue = parseValue(value);
		String stringValue = null;
		if (decimalValue != null) {
			stringValue = decimalValue.toPlainString();
		}
		vaadinFld.setValue(stringValue);
	}

	@Override
	public Object getValue() {
		DecimalField vaadinFld = (DecimalField) getVaadinField();
		Object value = vaadinFld.getValue();
		BigDecimal decimalValue = parseValue(value);
		return decimalValue;
	}

	private BigDecimal parseValue(Object value) {
		BigDecimal decimalValue = null;
		if (value != null) {
			decimalValue = new BigDecimal("" + value);
		}
		return decimalValue;
	}

}
