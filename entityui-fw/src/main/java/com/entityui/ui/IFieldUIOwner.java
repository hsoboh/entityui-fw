package com.entityui.ui;


public interface IFieldUIOwner {

	public void fieldChanged(String fldId, Object newValue);

}
