package com.entityui.ui.manager;


public abstract class ApplicationConfig {

	private DatabaseConfig databaseConfig;
	private EntityConfig entityConfig;

	public ApplicationConfig() {
		this.databaseConfig = new DatabaseConfig();
		buildDatabaseConfig(this.databaseConfig);

		this.entityConfig = new EntityConfig();
		buildEntityConfig(this.entityConfig);
	}

	public abstract void buildDatabaseConfig(DatabaseConfig config);

	public abstract void buildEntityConfig(EntityConfig config);

	DatabaseConfig getDatabaseConfig() {
		return this.databaseConfig;
	}

	EntityConfig getEntityConfig() {
		return this.entityConfig;
	}

}
