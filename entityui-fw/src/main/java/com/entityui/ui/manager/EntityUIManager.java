package com.entityui.ui.manager;

import java.util.List;

import com.entityui.entity.Entity;
import com.entityui.entity.PersistanceConfig;
import com.entityui.entity.PersistenceManager;
import com.entityui.entity.PersistenceUnit;
import com.entityui.entity.Restriction;
import com.entityui.entity.Select;
import com.entityui.entity.User;

public class EntityUIManager {

	private static EntityUIManager instance;
	private EntityConfig entityConfig;
	private DatabaseConfig databaseConfig;

	private EntityUIManager() {

	}

	public static EntityUIManager getInstance() {
		if (instance == null) {
			instance = new EntityUIManager();
		}
		return instance;
	}

	public void init(ApplicationConfig desc) throws Exception {
		//Initialize DB
		this.databaseConfig = desc.getDatabaseConfig();
		this.entityConfig = desc.getEntityConfig();

		PersistanceConfig config = new PersistanceConfig();
		config.setConnectionUrl(this.databaseConfig.getConnectionUrl());
		config.setDbUser(this.databaseConfig.getDbUser());
		config.setDbPassword(this.databaseConfig.getDbPassword());

		this.entityConfig.registerEntityUnit(new EntityUnit(User.class));
		
		for (EntityUnit unit : this.entityConfig.getUnits()) {
			PersistenceUnit persistenceUnit = new PersistenceUnit(unit.getEntity());
			config.registerEntityUnit(persistenceUnit);
		}

		PersistenceManager manager = PersistenceManager.getInstance();
		manager.applyConfigs(config);
		
		//Create System user
		Select select = new Select(User.class);
		select.addRestriction(Restriction.eq(User.FIELD_USERNAME, User.SYSTEM));
		
		List<? extends Entity> users = manager.executeSelect(select);
		if (users == null || users.isEmpty()) {
			User user = (User) PersistenceManager.getEntitySchema(User.class).newEntity();
			user.setField(User.FIELD_USERNAME, User.SYSTEM);
			user.setField(User.FIELD_PASSWORD, "123456");

			user.save();
		}
	}

	public EntityConfig getEntityConfig() {
		return this.entityConfig;
	}

	public DatabaseConfig getDatabaseConfig() {
		return this.databaseConfig;
	}
}
