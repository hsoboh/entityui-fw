package com.entityui.ui.manager;

import java.util.ArrayList;
import java.util.List;

import com.entityui.entity.Entity;

public class EntityConfig {
	private List<EntityUnit> units = new ArrayList<EntityUnit>();

	public void registerEntityUnit(EntityUnit unit) {
		if (containsUnit(unit.getEntity())) {
			throw new IllegalArgumentException("Another unit exist with the same entity = " + unit.getEntity());
		}
		this.units.add(unit);
	}

	private boolean containsUnit(Class<? extends Entity> entity) {
		for (EntityUnit unit : this.units) {
			if (unit.getEntity().equals(entity)) {
				return true;
			}
		}
		return false;
	}

	public List<EntityUnit> getUnits() {
		return this.units;
	}

	public EntityUnit getUnit(Class<? extends Entity> entity) {
		for (EntityUnit unit : this.units) {
			if (unit.getEntity().equals(entity)) {
				return unit;
			}
		}
		return null;
	}

}
