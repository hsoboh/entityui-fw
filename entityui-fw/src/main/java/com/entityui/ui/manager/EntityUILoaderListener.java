package com.entityui.ui.manager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EntityUILoaderListener implements ServletContextListener {

	private static final String INIT_PARAM_APP_CONFIG = "app-config";

	private final static Logger logger = LogManager.getLogger(EntityUILoaderListener.class);

	public void contextInitialized(ServletContextEvent sce) {
		// Get ApplicationDescriptor parameter
		String className = sce.getServletContext().getInitParameter(INIT_PARAM_APP_CONFIG);

		// create instance
		try {
			Class<? extends ApplicationConfig> clazz = (Class<? extends ApplicationConfig>) Class.forName(className);
			ApplicationConfig desc = clazz.newInstance();
			EntityUIManager.getInstance().init(desc);
		} catch (Exception e) {
			logger.fatal(e);
			throw new RuntimeException(e);
		}
	}

	public void contextDestroyed(ServletContextEvent sce) {

	}

}
