package com.entityui.ui.manager;

public class EntityAction {

	private String id;
	private String caption;

	public EntityAction() {

	}

	public EntityAction(String id, String caption) {
		this.id = id;
		this.caption = caption;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCaption() {
		return this.caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public boolean isAction(String id) {
		return this.id.equals(id);
	}

	@Override
	public final boolean equals(Object obj) {
		if (EntityAction.class.isAssignableFrom(obj.getClass())) {
			return getId() == ((EntityAction) obj).getId();
		}
		return super.equals(obj);
	}

}
