package com.entityui.ui.manager;

import com.entityui.entity.Entity;

public class EntityUnit {

	private final Class<? extends Entity> entity;

	public EntityUnit(Class<? extends Entity> entity) {
		this.entity = entity;
	}

	public Class<? extends Entity> getEntity() {
		return this.entity;
	}
}
