package com.entityui.ui;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.tepi.filtertable.FilterTable;

import com.entityui.entity.Entity;
import com.entityui.entity.EntityChangeListener;
import com.entityui.entity.Field;
import com.entityui.entity.FieldEntity;
import com.entityui.entity.PersistenceManager;
import com.entityui.entity.validation.ValidationException;
import com.entityui.ui.manager.EntityAction;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomTable.RowHeaderMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

public class EntityListPanel extends Panel implements IEntityListView, EntityChangeListener {

	private static final long serialVersionUID = -6117820630426865268L;

	private FilterTable table;
	private Label labelTotal;
	private final List<Button> actionButtons;
	private final Entity listEntity;

	public EntityListPanel(Entity listEntity) {
		this.listEntity = listEntity;
		this.actionButtons = new ArrayList<Button>();
		PersistenceManager manager = PersistenceManager.getInstance();
		manager.addListener(this, listEntity.getClass());
	}

	public void init() {
		initComponents();
		initUI();
		reloadData();
		enableDisableActions();
	}

	private void reloadData() {
		try {
			this.listEntity.doAction(Entity.ACTION_REFRESH);
		} catch (ValidationException e) {
			GlobalErrorHandler.showValidationError(e);
		}
	}

	public void initComponents() {

		for (final EntityAction action : this.listEntity.getListActions()) {
			Button button = new Button(action.getCaption());
			button.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				public void buttonClick(ClickEvent event) {
					try {
						EntityListPanel.this.listEntity.doAction(action);
					} catch (ValidationException e) {
						GlobalErrorHandler.showValidationError(e);
					}
				}

			});
			// button.setIcon(new ThemeResource("img/" + action.getIcon()));
			button.setData(action);
			this.actionButtons.add(button);
		}

		this.table = new FilterTable();
		this.table.setSelectable(true);
		this.table.setImmediate(true);
		this.table.setFilterBarVisible(true);
		this.table.setFooterVisible(false);
		// this.table.setFilterGenerator(new EntityFilterGenerator(this));
		// this.table.setFilterDecorator(new EntityFilterDecorator());
		this.table.setContainerDataSource(buildContainer());

		this.table.setRowHeaderMode(RowHeaderMode.INDEX);
		this.table.setColumnWidth(null, 25);
		this.table.setColumnCollapsingAllowed(true);
		this.table.setColumnReorderingAllowed(true);

		this.table.setNullSelectionAllowed(false);
		this.table.addItemClickListener(new ItemClickEvent.ItemClickListener() {

			private static final long serialVersionUID = -8434522735036745322L;

			public void itemClick(ItemClickEvent event) {
				if (event.isDoubleClick()) {
					// EntityListPanel.this.dummyEntity.doAction(EntityListPanel.this.dummyEntity.findAction(Entity.ACTION_OPEN));
				}
			}
		});
		// Handle selection change.
		this.table.addValueChangeListener(new Property.ValueChangeListener() {

			private static final long serialVersionUID = 3370663722915891362L;

			public void valueChange(ValueChangeEvent event) {
				selectionChanged();
			}
		});

		this.labelTotal = new Label();
		this.labelTotal.addStyleName("entitiesListTotal");
	}

	private Container buildContainer() {

		Container cont = new IndexedContainer();
		for (Field fld : this.listEntity.getFields()) {
			if (!fld.isHidden()) {
				cont.addContainerProperty(fld.getCaption(), EntityToUIUtil.getFieldVewClass(fld), null);
			}
		}

		return cont;
	}

	private void initUI() {
		setSizeFull();

		VerticalLayout rootLayout = new VerticalLayout();
		rootLayout.setSpacing(true);
		rootLayout.setMargin(true);
		rootLayout.setSizeFull();
		setContent(rootLayout);

		HorizontalLayout toolbar = new HorizontalLayout();
		toolbar.setSpacing(true);
		for (Button button : this.actionButtons) {
			toolbar.addComponent(button);
		}
		rootLayout.addComponent(toolbar);

		this.table.setSizeFull();
		this.table.addStyleName(Reindeer.TABLE_STRONG);
		rootLayout.addComponent(this.table);
		rootLayout.setExpandRatio(this.table, 1.0f);
		rootLayout.addComponent(this.labelTotal);

	}

	private void enableDisableActions() {
		for (Button button : this.actionButtons) {
			EntityAction action = (EntityAction) button.getData();
			button.setEnabled(this.listEntity.isEnabled(action));
		}
	}

	private void selectionChanged() {
		enableDisableActions();
	}

	private void reCalculateTotal() {
		DecimalFormat formatter = new DecimalFormat("#,###");
		long size = this.table.size();
		String total = formatter.format(size);
		this.labelTotal.setValue("Total: " + total);
	}

	public Object getSelectedEntities() {
		return this.table.getValue();
	}

	public void setEntities(List<? extends Entity> entities) {
		this.table.getContainerDataSource().removeAllItems();

		Container cont = this.table.getContainerDataSource();

		for (Entity entity : entities) {
			Item item = cont.addItem(entity.getId());
			for (Field fld : this.listEntity.getFields()) {
				if (!fld.isHidden()) {
					Object value = entity.getField(fld);
					if (fld.getClass().equals(FieldEntity.class) && value != null) {
						FieldEntity<?> fldEntity = (FieldEntity<?>) fld;
						Field lookupField = fldEntity.getLookupField();
						Entity lookupEntity = (Entity) value;
						value = lookupEntity.getField(lookupField);
					}
					item.getItemProperty(fld.getCaption()).setValue(value);
				}
			}
		}

		reCalculateTotal();

	}

	public Entity getListEntity() {
		return this.listEntity;
	}

	public void entityAdded(Entity entity) {
		reloadData();
	}

	public void entityDeleted(long id) {
		reloadData();
	}

	public void entityUpdated(Entity entity) {
		reloadData();
	}

	@Override
	public void detach() {
		super.detach();
		PersistenceManager manager = PersistenceManager.getInstance();
		manager.removeListener(this, this.listEntity.getClass());
	}
}
