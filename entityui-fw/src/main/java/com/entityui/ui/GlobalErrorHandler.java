package com.entityui.ui;


import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.entityui.entity.validation.ValidationException;
import com.vaadin.server.AbstractErrorMessage.ContentMode;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.ErrorEvent;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class GlobalErrorHandler implements ErrorHandler {

	private static final long serialVersionUID = 2100592418683291081L;

	private final static Logger logger = LogManager.getLogger(GlobalErrorHandler.class);

	public void error(ErrorEvent event) {
		logger.error(event.getThrowable() + " " + ExceptionUtils.getStackTrace(event.getThrowable()));
		 // Finds the original source of the error/exception
	    AbstractComponent component = DefaultErrorHandler.findAbstractComponent(event);
	    if (component != null) {
			String message = "Internal Error!";
			ErrorMessage errorMessage = new UserError(message, ContentMode.TEXT,
					ErrorMessage.ErrorLevel.ERROR);
			if (errorMessage != null) {
				component.setComponentError(errorMessage);
	            new Notification(null, errorMessage.getFormattedHtmlMessage(), Type.WARNING_MESSAGE, true).show(Page.getCurrent());
	            return;
	        }
	    }
	    DefaultErrorHandler.doDefault(event);
	}

	public static void showValidationError(ValidationException e) {
		ErrorMessage errorMessage = new UserError(e.getValidationMessage(), ContentMode.TEXT, ErrorMessage.ErrorLevel.ERROR);
		new Notification(null, errorMessage.getFormattedHtmlMessage(), Type.WARNING_MESSAGE, true).show(Page.getCurrent());
	}
}
