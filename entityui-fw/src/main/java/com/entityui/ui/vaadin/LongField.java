package com.entityui.ui.vaadin;


public class LongField extends com.entityui.ui.vaadin.TextField {

	private static final long serialVersionUID = 5504094803229331940L;

	public LongField(String caption) {
		super(caption, "\\d*");
	}
}