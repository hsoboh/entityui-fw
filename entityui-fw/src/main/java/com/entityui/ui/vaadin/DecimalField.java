package com.entityui.ui.vaadin;

public class DecimalField extends TextField {

	private static final long serialVersionUID = -4148385353118220997L;

	public DecimalField(String caption, int precision, int scale) {
		super(caption);

		int left = precision - scale;
		// String regex = "\\d{" + 0 + "," + left + "}.?\\d{" + 0 + "," + scale
		// + "}";
		String regex = "\\d*.?\\d*";
		setRegex(regex);

		String inputPromt = "";
		for (int i = 0; i < left; i++) {
			inputPromt += "0";
		}
		inputPromt += ".";
		for (int i = 0; i < scale; i++) {
			inputPromt += "0";
		}

		setInputPrompt(inputPromt);
	}

}
