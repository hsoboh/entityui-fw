package com.entityui.ui.vaadin;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;

public class TextField extends com.vaadin.ui.TextField implements TextChangeListener {

	private static final long serialVersionUID = -1869616288420751568L;

	private String lastValue;

	private String regex;

	public TextField(String caption) {
		this(caption, null);
	}

	public TextField(String caption, String regex) {
		super(caption);
		this.regex = regex;
		setImmediate(true);
		setTextChangeEventMode(TextChangeEventMode.EAGER);
		addTextChangeListener(this);
	}

	public void textChange(TextChangeEvent event) {
		String text = event.getText();

		if (this.regex != null) {
			Pattern pattern = Pattern.compile(this.regex);

			Matcher matcher = pattern.matcher(text);

			if (matcher.matches()) {
				String matchValue = matcher.group();
				// setValue(matchValue);
				this.lastValue = matchValue;
			} else {
				setValue(this.lastValue);
			}
		}
	}

	public String getRegex() {
		return this.regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

}
