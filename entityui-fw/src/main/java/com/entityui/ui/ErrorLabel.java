package com.entityui.ui;

import com.vaadin.server.UserError;
import com.vaadin.ui.Label;

class ErrorLabel extends Label {
	private static final long serialVersionUID = 2234057247768494655L;

	public ErrorLabel() {
		setVisible(false);
	}

	public void setError(String error) {
		setValue(error);
		setComponentError(new UserError(error));
		setVisible(true);
	}

    public void clearError() {
		setValue(null);
		setComponentError(null);
		setVisible(false);
	}
}