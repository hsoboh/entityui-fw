package com.entityui.ui;

import java.util.List;

import com.entityui.entity.Entity;

public interface IEntityListView {

	public Object getSelectedEntities();

	public void setEntities(List<? extends Entity> entities);

}
