package com.entityui.ui;

import java.util.ArrayList;
import java.util.List;

import com.entityui.entity.Entity;
import com.entityui.entity.Field;
import com.entityui.entity.validation.ValidationException;
import com.entityui.ui.manager.EntityAction;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

public class EntityWindow extends Window implements IFieldUIOwner, IEntityView {

	private static final long serialVersionUID = -7959539335058621930L;

	private final Entity entity;

	private List<FieldFieldUIPair> fields;

	private ArrayList<Button> actionButtons;

	private boolean initializing = false;

	public EntityWindow(Entity entity) {
		this.entity = entity;

		this.fields = new ArrayList<EntityWindow.FieldFieldUIPair>();

		this.actionButtons = new ArrayList<Button>();

		this.initializing = true;
	}

	public void init() {
		try {

			for (Field fld : this.entity.getFields()) {
				if (fld.isHidden()) {
					continue;
				}
				FieldUI ui = EntityToUIUtil.createFieldUI(fld, this);
				this.fields.add(new FieldFieldUIPair(fld, ui));
			}

			initComponents();
			initUI();

			readEntity();

			setCaption(this.entity.buildEntityCaption());

			// setWidth("500");
			// setHeight("500");
			center();

			setClosable(true);
			setResizable(true);
			enableDisableActions();
			enableDisableFields();
		} finally {
			this.initializing = false;
		}
	}

	public void initComponents() {

		for (final EntityAction action : this.entity.getEntityActions()) {
			Button button = new Button(action.getCaption());
			button.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 142877247782916038L;

				public void buttonClick(ClickEvent event) {
					try {
						EntityWindow.this.entity.doAction(action);
					} catch (ValidationException e) {
						GlobalErrorHandler.showValidationError(e);
					}
				}
			});
			button.setData(action);
			this.actionButtons.add(button);
		}
	}

	// private void initUI() {
	// VerticalLayout rootLayout = new VerticalLayout();
	// rootLayout.setMargin(new MarginInfo(false, true, true, true));
	// rootLayout.setSizeFull();
	//
	// HorizontalLayout toolbarLayout = new HorizontalLayout();
	// toolbarLayout.setHeight("30px");
	// toolbarLayout.setSpacing(true);
	// for (Button button : this.actionButtons) {
	// toolbarLayout.addComponent(button);
	// toolbarLayout.setComponentAlignment(button, Alignment.MIDDLE_LEFT);
	// }
	//
	// Panel bodyPanel = buildBodyPanel();
	// bodyPanel.setWidth("100%");
	// bodyPanel.addStyleName(Reindeer.PANEL_LIGHT);
	// bodyPanel.setSizeFull();
	//
	// VerticalSplitPanel split = new VerticalSplitPanel();
	// split.setFirstComponent(toolbarLayout);
	// split.setSecondComponent(bodyPanel);
	// split.setLocked(true);
	// split.setSplitPosition(30, Unit.PIXELS);
	// rootLayout.addComponent(split);
	//
	// rootLayout.setExpandRatio(split, 1.0f);
	//
	// setContent(rootLayout);
	// }

	private void initUI() {
		VerticalLayout rootLayout = new VerticalLayout();
		rootLayout.setMargin(new MarginInfo(false, true, true, true));
		rootLayout.setWidth(null);

		HorizontalLayout toolbarLayout = new HorizontalLayout();
		toolbarLayout.setHeight("30px");
		toolbarLayout.setWidth(null);
		toolbarLayout.setSpacing(true);
		for (Button button : this.actionButtons) {
			toolbarLayout.addComponent(button);
			toolbarLayout.setComponentAlignment(button, Alignment.MIDDLE_LEFT);
		}

		// Label seperator = new Label("<hr />", ContentMode.HTML);
		// seperator.setWidth(null);

		Panel bodyPanel = buildBodyPanel();
		bodyPanel.setWidth(null);
		bodyPanel.addStyleName(Reindeer.PANEL_LIGHT);
		// bodyPanel.setSizeFull();

		rootLayout.addComponent(toolbarLayout);
		// rootLayout.addComponent(seperator);
		rootLayout.addComponent(bodyPanel);

		//
		// VerticalSplitPanel split = new VerticalSplitPanel();
		// // split.setMargin(new MarginInfo(false, true, true, true));
		// split.setFirstComponent(toolbarLayout);
		// split.setSecondComponent(bodyPanel);
		// split.setLocked(false);
		// // split.setSplitPosition(30, Unit.PIXELS);
		// split.setWidth(null);
		//
		// setContent(split);
		// rootLayout.addComponent(split);
		//
		// rootLayout.setExpandRatio(split, 1.0f);

		setContent(rootLayout);

	}

	protected Panel buildBodyPanel() {
		Panel panel = new Panel();
		FormLayout layout = new FormLayout();
		for (FieldFieldUIPair pair : this.fields) {
			FieldUI ui = pair.getFieldUI();
			AbstractField vaadinUI = ui.getVaadinField();
			layout.addComponent(vaadinUI);
		}

		panel.setContent(layout);

		return panel;
	}

	private void enableDisableActions() {
		for (Button button : this.actionButtons) {
			EntityAction action = (EntityAction) button.getData();
			button.setEnabled(this.entity.isEnabled(action));
		}
	}

	private void enableDisableFields() {
		for (FieldFieldUIPair pair : this.fields) {
			FieldUI ui = pair.getFieldUI();
			Component vaadinUI = ui.getVaadinField();
			vaadinUI.setEnabled(this.entity.isEnabled(pair.getField()));
		}
	}

	private void readEntity() {
		for (FieldFieldUIPair pair : this.fields) {
			FieldUI ui = pair.getFieldUI();
			Object value = this.entity.getField(pair.getField().getId());
			ui.setValue(value);
		}
	}

	public void closeUI() {
		close();
	}

	public Entity getEntity() {
		return this.entity;
	}

	public void fieldChanged(String fldId, Object newValue) {
		if (!this.initializing) {
			Field fld = findFieldById(fldId);
			this.entity.setField(fld.getId(), newValue);
		}

		enableDisableFields();
		enableDisableActions();

		for (FieldFieldUIPair pair : this.fields) {
			try {
				this.entity.validate(pair.getField());
				pair.getFieldUI().markValid();
			} catch (ValidationException e) {
				pair.getFieldUI().markInvalid(e.getValidationMessage());
			}
		}
	}

	public void setField(String fldId, Object value) {
		// trigger change only if value not same, to avoid infinite loop
		Object viewValue = getField(fldId);
		if (!Field.equals(value, viewValue)) {
			FieldUI ui = findFieldUIById(fldId);
			ui.setValue(value);
		}
	}

	public Object getField(String fldId) {
		FieldUI ui = findFieldUIById(fldId);
		return ui.getValue();
	}

	private Field findFieldById(String id) {
		for (FieldFieldUIPair pair : this.fields) {
			Field fld = pair.getField();
			if (fld.getId().equals(id)) {
				return fld;
			}
		}
		throw new IllegalArgumentException("Should not happen, not found field with id " + id);
	}

	private FieldUI findFieldUIById(String id) {
		for (FieldFieldUIPair pair : this.fields) {
			FieldUI ui = pair.getFieldUI();
			if (ui.getFld().getId().equals(id)) {
				return ui;
			}
		}
		throw new IllegalArgumentException("Should not happen, not found fieldUI with id " + id);
	}

	class FieldFieldUIPair {
		private Field field;
		private FieldUI fieldUI;

		public FieldFieldUIPair(Field field, FieldUI fieldUI) {
			super();
			this.field = field;
			this.fieldUI = fieldUI;
		}

		public FieldUI getFieldUI() {
			return this.fieldUI;
		}

		public void setFieldUI(FieldUI fieldUI) {
			this.fieldUI = fieldUI;
		}

		public Field getField() {
			return this.field;
		}

	}
}
