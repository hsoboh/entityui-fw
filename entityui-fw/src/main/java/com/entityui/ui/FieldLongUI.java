package com.entityui.ui;

import com.entityui.entity.Field;
import com.entityui.ui.vaadin.LongField;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.AbstractField;

public class FieldLongUI extends FieldUI {

	public FieldLongUI(Field fld, String caption, IFieldUIOwner owner) {
		super(fld, caption, owner);
	}

	@Override
	protected AbstractField createVaadinField() {
		LongField editFld = new LongField(getCaption());
		editFld.setImmediate(true);
		editFld.setNullRepresentation("");
		editFld.setNullSettingAllowed(true);
		editFld.setRequired(!getFld().isNullable());
		editFld.addValueChangeListener(new Property.ValueChangeListener() {
			private static final long serialVersionUID = -2490511611758325512L;

			public void valueChange(ValueChangeEvent event) {
				Object value = event.getProperty().getValue();
				getOwner().fieldChanged(getFld().getId(), parseValue(value));
			}
		});
		return editFld;
	}

	@Override
	public void setValue(Object value) {
		LongField vaadinFld = (LongField) getVaadinField();
		Long longValue = parseValue(value);
		String stringValue = null;
		if (longValue != null) {
			stringValue = String.valueOf(longValue);
		}
		vaadinFld.setValue(stringValue);
	}

	@Override
	public Object getValue() {
		LongField vaadinFld = (LongField) getVaadinField();
		Object value = vaadinFld.getValue();
		Long longValue = parseValue(value);
		return longValue;
	}

	private Long parseValue(Object value) {
		Long longValue = null;
		if (value != null) {
			longValue = Long.parseLong("" + value);
		}
		return longValue;
	}
}
