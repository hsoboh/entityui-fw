package com.entityui.ui;

public interface IEntityView {

	public void closeUI();

	public void setField(String fldId, Object value);

	public Object getField(String fldId);

}
