package com.entityui.ui;

import java.math.BigDecimal;
import java.util.List;

import com.entityui.entity.Entity;
import com.entityui.entity.Field;
import com.entityui.entity.FieldDecimal;
import com.entityui.entity.FieldEntity;
import com.entityui.entity.FieldLong;
import com.entityui.entity.FieldText;
import com.entityui.entity.PersistenceManager;

public class EntityToUIUtil {

	public static Class<?> getFieldVewClass(Field fld) {

		if (fld.getClass().equals(FieldLong.class)) {
			return Long.class;
		}
		if (fld.getClass().equals(FieldText.class)) {
			return String.class;
		}
		if (fld.getClass().equals(FieldDecimal.class)) {
			return BigDecimal.class;
		}
		if (fld.getClass().equals(FieldEntity.class)) {
			FieldEntity<?> fldEntity = (FieldEntity<?>) fld;
			Field lookupField = fldEntity.getLookupField();
			return getFieldVewClass(lookupField);
		}
		throw new IllegalArgumentException("Not handled field " + fld);
	}

	public static FieldUI createFieldUI(Field fld, IFieldUIOwner owner) {
		if (fld.getClass().equals(FieldText.class)) {
			FieldText fldText = (FieldText)fld;
			return new FieldTextUI(fld, fld.getCaption(), owner, fldText.getMask());
		}
		if (fld.getClass().equals(FieldLong.class)) {
			return new FieldLongUI(fld, fld.getCaption(), owner);
		}
		if (fld.getClass().equals(FieldDecimal.class)) {
			FieldDecimal fldDecimal = (FieldDecimal) fld;
			return new FieldDecimalUI(fld, fld.getCaption(), owner, fldDecimal.getPrecision(), fldDecimal.getScale());
		}
		if (fld.getClass().equals(FieldEntity.class)) {
			FieldEntity<?> fldEntity = (FieldEntity<?>) fld;
			List<? extends Entity> entities = PersistenceManager.getInstance().loadEntities(fldEntity.getEntity());
			return new FieldEntityUI(fldEntity, fld.getCaption(), owner, entities);
		}
		throw new IllegalArgumentException("Not handled field type " + fld);
	}
}
