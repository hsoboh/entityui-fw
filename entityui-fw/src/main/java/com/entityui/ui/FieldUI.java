package com.entityui.ui;

import com.entityui.entity.Field;
import com.vaadin.ui.AbstractField;

public abstract class FieldUI {

	private AbstractField vaadinField;
	private IFieldUIOwner owner;
	private String caption;
	private Field fld;

	public FieldUI(Field fld, String caption, IFieldUIOwner owner) {
		this.fld = fld;
		this.caption = caption;
		this.owner = owner;
	}

	protected abstract AbstractField createVaadinField();

	public abstract void setValue(Object value);

	public abstract Object getValue();

	public final AbstractField getVaadinField() {
		if (this.vaadinField == null) {
			// This was in constructor, but moved here to make sure regex is set
			// in subclass(TextFieldUI)
			this.vaadinField = createVaadinField();
		}
		return this.vaadinField;
	}

	public IFieldUIOwner getOwner() {
		return this.owner;
	}

	public String getCaption() {
		return this.caption;
	}

	public Field getFld() {
		return this.fld;
	}

	public void markValid() {
		this.vaadinField.setStyleName("field-valid");
		this.vaadinField.setDescription(null);
	}

	public void markInvalid(String message) {
		this.vaadinField.setStyleName("field-invalid");
		this.vaadinField.setDescription(message);
	}
}
