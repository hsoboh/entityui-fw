package com.entityui.entity;

public class FieldText extends Field {

	private String mask;

	public FieldText(String id) {
		super(id);
	}

	public FieldText(String id, String caption) {
		super(id, caption);
	}

	@Override
	public boolean isNull(Object value) {
		if (super.isNull(value)) {
			return true;
		}
		String str = String.valueOf(value);
		return str.isEmpty();
	}

	public String getMask() {
		return this.mask;
	}

	public void setMask(String mask) {
		this.mask = mask;
	}

	@Override
	public Object wrap(Object object) {
		if (object == null) {
			return null;
		}
		String stringValue = (String) object;
		return stringValue;
	}

}
