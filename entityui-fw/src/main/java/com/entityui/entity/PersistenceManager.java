package com.entityui.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.entityui.entity.hibernate.HibernateAdapter;
import com.entityui.entity.hibernate.HibernateConfig;

public class PersistenceManager {

	private List<EntitySchema> entitySchemas = new ArrayList<EntitySchema>();
	
	private String connectionUrl;
	private String dbUser;
	private String dbPassword;

	private static PersistenceManager instance;

	private boolean configsApplied = false;

	private Map<Class<? extends Entity>, List<EntityChangeListener>> listeners;

	private PersistenceManager() {
		this.listeners = new HashMap<Class<? extends Entity>, List<EntityChangeListener>>();

	}

	public static PersistenceManager getInstance() {
		if (instance == null) {
			instance = new PersistenceManager();
		}
		return instance;
	}

	public void applyConfigs(PersistanceConfig config) {
		if (this.configsApplied) {
			throw new IllegalStateException("Persistence already configured");
		}

		if (config.getConnectionUrl() == null) {
			throw new IllegalArgumentException("Configs missing connection url");
		}
		if (config.getDbUser() == null) {
			throw new IllegalArgumentException("Configs missing db user");
		}
		if (config.getDbPassword() == null) {
			throw new IllegalArgumentException("Configs missing db password");
		}

		this.connectionUrl = config.getConnectionUrl();
		this.dbUser = config.getDbUser();
		this.dbPassword = config.getDbPassword();

		for (PersistenceUnit unit : config.getUnits()) {
			addEntity(unit.getEntity());
		}

		configureHibernate();

		this.configsApplied = true;
	}

	private void configureHibernate() {
		HibernateConfig config = new HibernateConfig();
		config.setConnectionUrl(this.connectionUrl);
		config.setDbUser(this.dbUser);
		config.setDbPassword(this.dbPassword);

		for (EntitySchema schema : this.entitySchemas) {
			config.addEntitySchema(schema);
		}

		HibernateAdapter.getInstance().applyConfig(config);
	}

	private void addEntity(Class<? extends Entity> entityClass) {
		Entity initializingEntity = null;
		try {
			initializingEntity = entityClass.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		EntitySchema schema = new EntitySchema();
		initializingEntity.buildSchema(schema);

		schema.setEntityClass(entityClass);

		this.entitySchemas.add(schema);
	}

	public static EntitySchema getEntitySchema(Class<? extends Entity> entityClass){
		for (EntitySchema schema : getInstance().entitySchemas) {
			if (schema.getEntityClass().equals(entityClass)) {
				return schema;
			}
		}
		return null;
	}

	void save(Entity entity) {
		boolean add = entity.isNew();
		if (add) {
			long id = HibernateAdapter.getInstance().saveNew(entity);
			entity.setField(Entity.FIELD_ID, id);
		} else {
			HibernateAdapter.getInstance().saveUpdate(entity);
		}

		if (this.listeners.containsKey(entity.getClass())) {
			for (EntityChangeListener lsnr : this.listeners.get(entity.getClass())) {
				if (add) {
					lsnr.entityAdded(entity);
				} else {
					lsnr.entityUpdated(entity);
				}
			}
		}
	}
	
	public void delete(Entity entity) {
		HibernateAdapter.getInstance().delete(entity);
		if (this.listeners.containsKey(entity.getClass())) {
			for (EntityChangeListener lsnr : this.listeners.get(entity.getClass())) {
				lsnr.entityDeleted(entity.getId());
			}
		}
	}

	public Entity load(long id, Class<? extends Entity> entityClass){
		EntitySchema schema = getEntitySchema(entityClass);
		return HibernateAdapter.getInstance().load(id, schema);
	}

	// public static EntitySchema getEntitySchema(Class<? extends EntitySchema>
	// entitySchemaClass) {
	// for (EntitySchema schema : getInstance().entitySchemas) {
	// if (schema.getClass().equals(entitySchemaClass)) {
	// return schema;
	// }
	// }
	// return null;
	// }

	public List<? extends Entity> loadEntities(Class<? extends Entity> entityClass) {
		Select select = new Select(entityClass);
		return executeSelect(select);
	}

	public List<? extends Entity> executeSelect(Select select) {
		return HibernateAdapter.getInstance().executeSelect(select);
	}

	public void addListener(EntityChangeListener listener, Class<? extends Entity> entity) {
		if (!this.listeners.containsKey(entity)) {
			List<EntityChangeListener> list = new ArrayList<EntityChangeListener>();
			this.listeners.put(entity, list);
		}

		this.listeners.get(entity).add(listener);
	}

	public void removeListener(EntityChangeListener listener, Class<? extends Entity> entity) {
		List<EntityChangeListener> list = this.listeners.get(entity);
		list.remove(listener);
	}
}
