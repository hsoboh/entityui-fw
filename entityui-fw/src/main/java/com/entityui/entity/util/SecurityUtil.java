package com.entityui.entity.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

public class SecurityUtil {

	public static String doHash(String stringToHash)
			throws NoSuchAlgorithmException {

		MessageDigest md = MessageDigest.getInstance("MD5");

		byte[] dst = md.digest(stringToHash.getBytes());

		byte[] hashed = Base64.encodeBase64(dst);

		return new String(hashed);
	}
}
