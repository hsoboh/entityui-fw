package com.entityui.entity;

import java.util.ArrayList;
import java.util.List;

public class Select {

	private Class<? extends Entity> entityClass;

	private List<Restriction> restrictions = new ArrayList<Restriction>();
	
	public Select(Class<? extends Entity> entityClass) {
		this.entityClass = entityClass;
	}

	public Class<? extends Entity> getEntityClass() {
		return this.entityClass;
	}

	public void setEntityClass(Class<? extends Entity> entityClass) {
		this.entityClass = entityClass;
	}

	public EntitySchema getEntitySchema() {
		return PersistenceManager.getEntitySchema(this.entityClass);
	}
	
	public void addRestriction(Restriction restriction){
		this.restrictions.add(restriction);
	}

	public List<Restriction> getRestrictions() {
		return this.restrictions;
	}
}