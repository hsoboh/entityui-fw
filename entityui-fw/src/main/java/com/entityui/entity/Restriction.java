package com.entityui.entity;


public class Restriction {

	private String fldId;
	
	private OPT operation;

	private Object value;

	private String leafFieldId;

	private String[] fullFieldPath;

	private boolean association;

	private Restriction(String fldId, OPT operation, Object value) {
		this.fldId = fldId;
		this.operation = operation;
		this.value = value;

		if (this.fldId.contains(".")) { // Association
			this.association = true;
		} else {
			this.association = false;
		}
		this.fullFieldPath = this.fldId.split("\\.");
		this.leafFieldId = this.fullFieldPath[this.fullFieldPath.length - 1];
	}
	
	public Restriction clone(String fieldId, Object value) {
		return new Restriction(fieldId, this.operation, value);
	}

	public static Restriction eq(String fldId, Object value) {
		return new Restriction(fldId, OPT.EQ, value);
	}

	public static Restriction ne(String fldId, Object value) {
		return new Restriction(fldId, OPT.NE, value);
	}

	public static Restriction contains(String fldId, String value) {
		return new Restriction(fldId, OPT.CONTAINS, value);
	}

	public String getFldId() {
		return this.fldId;
	}

	public OPT getOperation() {
		return this.operation;
	}

	public Object getValue() {
		return this.value;
	}

	public String getLeafFieldId() {
		return this.leafFieldId;
	}

	public String[] getFullFieldPath() {
		return this.fullFieldPath;
	}

	public boolean isAssociation() {
		return this.association;
	}

	@Override
	public String toString() {
		return "Field: [" + this.fldId + "], OPT: [" + this.operation + "], value: [" + this.value + "]";
	}

	public enum OPT {
		EQ, NE, CONTAINS
	}

}
