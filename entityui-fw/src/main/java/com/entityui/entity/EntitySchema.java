package com.entityui.entity;

import java.util.ArrayList;
import java.util.List;

public final class EntitySchema {

	private List<Field> fields = new ArrayList<Field>();

	private Class<? extends Entity> entityClass;

	protected EntitySchema() {

	}

	public final Class<? extends Entity> getEntityClass() {
		return this.entityClass;
	}

	final void setEntityClass(Class<? extends Entity> entityClass) {
		this.entityClass = entityClass;
	}

	public final void addField(Field field) {
		field.setEntitySchema(this);
		this.fields.add(field);
	}
	
	public List<Field> getFields() {
		return this.fields;
	}

	public Field getField(String id) {
		for(Field fld : this.fields){
			if(fld.isField(id)){
				return fld;
			}
		}
		return null;
	}

	public Entity newEntity() {
		Entity entity;
		try {
			entity = this.entityClass.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		entity.init(this);

		return entity;
	}

}
