package com.entityui.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.entityui.entity.validation.InvalidValueException;
import com.entityui.entity.validation.ValidationException;

public class FieldDecimal extends Field {

	private int precision = 15;// default value
	private int scale = 0;// default value
	private RoundingMode roundingMode = RoundingMode.HALF_EVEN;

	public FieldDecimal(String id) {
		this(id, null);
	}

	public FieldDecimal(String id, String caption) {
		super(id, caption);
	}

	public int getPrecision() {
		return this.precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public int getScale() {
		return this.scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}

	public RoundingMode getRoundingMode() {
		return this.roundingMode;
	}

	public void setRoundingMode(RoundingMode roundingMode) {
		this.roundingMode = roundingMode;
	}

	@Override
	public void validate(Entity entity) throws ValidationException {
		super.validate(entity);
		BigDecimal value = entity.getDecimal(this);
		if (value != null) {
			BigDecimal noZero = value.stripTrailingZeros();
			int valueScale = noZero.scale();
			int valuePrecision = noZero.precision();
			if (valueScale < 0) { // Adjust for negative scale
				valuePrecision -= valueScale;
				valueScale = 0;
			}
			if (valuePrecision - valueScale > this.precision - this.scale || valueScale > this.scale) {
				throw new InvalidValueException(entity, this, "Invalid precision or scale. Allowed precision = " + this.precision
						+ ", Allowed scale = " + this.scale);
			}
		}
	}

	@Override
	public Object wrap(Object object) {
		if(object == null){
			return null;
		}
		BigDecimal value = (BigDecimal)object;
		value = value.setScale(this.scale, this.roundingMode);
		return value;
	}
}
