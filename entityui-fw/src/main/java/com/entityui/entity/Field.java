package com.entityui.entity;

import java.util.List;

import com.entityui.entity.validation.UniqueViolationException;
import com.entityui.entity.validation.ValidationException;
import com.entityui.entity.validation.ValueMissingException;

public abstract class Field {

	private EntitySchema entitySchema;

	private final String id;
	private String caption;
	private boolean nullable = false; // default is not nullable
	private Object defaultValue;
	private boolean index;
	private boolean unique;
	private boolean hidden = false;
	private boolean enabled = true;

	public Field(String id, String caption) {
		this.id = id;
		this.caption = caption;
	}

	public Field(String id) {
		this(id, id);
	}

	public void validate(Entity entity) throws ValidationException {
		if (!isNullable() && isNull(entity.getField(this)) && !isField(Entity.FIELD_ID)) {
			throw new ValueMissingException(entity, this);
		}
		if (isUnique()) {
			PersistenceManager manage = PersistenceManager.getInstance();

			Select select = new Select(entity.getClass());
			select.addRestriction(Restriction.eq(this.id, entity.getField(this)));
			if (!entity.isNew()) {
				select.addRestriction(Restriction.ne(Entity.FIELD_ID, entity.getField(Entity.FIELD_ID)));
			}

			List<Entity> list = (List<Entity>) manage.executeSelect(select);
			if (list != null && !list.isEmpty()) {
				throw new UniqueViolationException(entity, this, list.get(0));
			}
		}
	}

	void setEntitySchema(EntitySchema schema) {
		this.entitySchema = schema;
	}

	public EntitySchema getEntitySchema() {
		return this.entitySchema;
	}

	public String getId() {
		return this.id;
	}

	public boolean isField(String id) {
		return this.id.equals(id);
	}

	public boolean isNullable() {
		return this.nullable;
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	public boolean isNull(Object value) {
		return value == null;
	}

	public Object getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}

	public boolean isIndex() {
		return this.index;
	}

	public void setIndex(boolean index) {
		this.index = index;
	}

	public boolean isUnique() {
		return this.unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	public String getCaption() {
		if (this.caption == null) {
			return this.id;
		} else {
			return this.caption;
		}
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public boolean isHidden() {
		return this.hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public static boolean equals(Object value1, Object value2) {
		if (value1 == null && value2 == null) {
			return true;
		}
		if (value1 != null) {
			return value1.equals(value2);
		} else {// value2 != null
			return value2.equals(value1);
		}
	}

	public abstract Object wrap(Object object);
}
