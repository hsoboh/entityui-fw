package com.entityui.entity;

import java.security.NoSuchAlgorithmException;

import com.entityui.entity.util.SecurityUtil;

public class User extends Entity {

	private static final long serialVersionUID = -440413519682931112L;

	public static final Object SYSTEM = "system";

	public static final String FIELD_USERNAME = "username";
	public static final String FIELD_PASSWORD = "password";

	@Override
	protected void buildSchema(EntitySchema schema) {
		super.buildSchema(schema);

		FieldText username = new FieldText(FIELD_USERNAME);
		username.setIndex(true);
		username.setUnique(true);
		username.setCaption("Username");
		schema.addField(username);

		FieldText password = new FieldText(FIELD_PASSWORD);
		password.setHidden(true);
		schema.addField(password);
	}

	@Override
	public void preSave() {
		super.preSave();

		if (isNew()) {// Password update only should happen in separate handling
			String passwordRaw = (String) getField(FIELD_PASSWORD);
			String pass = null;
			try {
				pass = SecurityUtil.doHash(passwordRaw);
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}
			setField(FIELD_PASSWORD, pass);
		}
	}

	@Override
	public String getHeadline() {
		return (String) getField(FIELD_USERNAME);
	}

	@Override
	public boolean isEnabled(Field fld) {
		if (fld.isField(FIELD_USERNAME) && !isNew()) {
			return false;
		}
		return super.isEnabled(fld);
	}
}
