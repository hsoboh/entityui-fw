package com.entityui.entity.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.w3c.dom.Document;

import com.entityui.entity.Entity;
import com.entityui.entity.EntitySchema;
import com.entityui.entity.Field;
import com.entityui.entity.FieldEntity;
import com.entityui.entity.PersistenceManager;
import com.entityui.entity.Restriction;
import com.entityui.entity.Select;

public class HibernateAdapter {

	private static HibernateAdapter instance;

	private boolean configsApplied = false;

	private List<EntitySchema> entitySchemas;

	private String connectionUrl;
	private String dbUser;
	private String dbPassword;

	private SessionFactory sessionFactory;

	public static HibernateAdapter getInstance() {
		if (instance == null) {
			instance = new HibernateAdapter();
		}
		return instance;
	}

	public void applyConfig(HibernateConfig config) {
		if (this.configsApplied) {
			throw new IllegalStateException("Hibernate already configured");
		}

		if (config.getConnectionUrl() == null) {
			throw new IllegalArgumentException("Configs missing connection url");
		}
		if (config.getDbUser() == null) {
			throw new IllegalArgumentException("Configs missing db user");
		}
		if (config.getDbPassword() == null) {
			throw new IllegalArgumentException("Configs missing db password");
		}

		this.connectionUrl = config.getConnectionUrl();
		this.dbUser = config.getDbUser();
		this.dbPassword = config.getDbPassword();

		this.entitySchemas = config.getEntitySchemas();

		this.sessionFactory = buildSessionFactory();

		this.configsApplied = true;
	}

	private SessionFactory buildSessionFactory() {
		try {
			Configuration configuration = new Configuration();

			// configuration.addAnnotatedClass(Department.class);

			configuration.setProperty("hibernate.connection.url", this.connectionUrl);
			configuration.setProperty("hibernate.connection.username", this.dbUser);
			configuration.setProperty("hibernate.connection.password", this.dbPassword);
			configuration.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
			configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
			configuration.setProperty("hibernate.hbm2ddl.auto", "create");
			configuration.setProperty("hibernate.show_sql", "false");
			configuration.setProperty("hibernate.format_sql", "true");
			configuration.setProperty("hibernate.connection.pool_size", "1");
			configuration.setProperty("hibernate.current_session_context_class", "thread");

			// File file = new File("sample-mapping.xml");
			// if (!file.exists()) {
			// throw new RuntimeException("File not found");
			// }

			EntitySchemaToDOM toDom = new EntitySchemaToDOM();
			Document doc = toDom.toDOM(this.entitySchemas);

			configuration.addDocument(doc);

			// String sampleMappingString = IOUtils.toString();
			// configuration.addInputStream(new FileInputStream(file));
			// FileReader fr = new FileReader(new File("sample-mapping.xml"));
			// fr.r
			//
			// configuration.add

			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
					.buildServiceRegistry();
			return configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public long saveNew(Entity entity) {

		Session session = this.sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		Map<String, Object> map = toHibernateMap(entity);

		Long id = (Long) session.save(entity.getClass().getSimpleName(), map);

		tx.commit();

		session.close();

		return id;
	}

	public void saveUpdate(Entity entity) {

		Session session = this.sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		Map<String, Object> map = toHibernateMap(entity);

		session.merge(entity.getClass().getSimpleName(), map);

		tx.commit();

		session.close();
	}

	public void delete(Entity entity) {

		Session session = this.sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		Map<String, Object> map = toHibernateMap(entity);

		session.delete(entity.getClass().getSimpleName(), map);

		tx.commit();

		session.close();
	}
	
	private Map<String, Object> toHibernateMap(Entity entity) {
		Map<String, Object> map = new HashMap<String, Object>();
		for (Field fld : entity.getSchema().getFields()) {
			map.put(fld.getId(), getHibernateValue(entity, fld));
		}
		return map;
	}

	private Object getHibernateValue(Entity entity, Field fld) {
		Object value = entity.getField(fld);
		if (value == null) {
			return null;
		}
		if (fld instanceof FieldEntity<?>) {
			Entity entityValue = entity.getEntity(fld);
			Map<String, Object> map = toHibernateMap(entityValue);

			return map;
		} else {
			return value;
		}
	}

	public Entity load(long id, EntitySchema schema) {
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(schema.getEntityClass().getSimpleName());
		criteria = criteria.add(Restrictions.eq(Entity.FIELD_ID, id));

		Map<String, Object> entityMap = (Map<String, Object>) criteria.list().get(0);
		Entity entity = populate(entityMap, schema);

		return entity;
		
	}

	private Entity populate(Map<String, Object> entityMap, EntitySchema schema) {
		Entity entity = schema.newEntity();
		for (Field fld : schema.getFields()) {
			entity.setField(fld.getId(), fromHibernateMap(fld, entityMap));
		}

		return entity;
	}

	private Object fromHibernateMap(Field fld, Map<String, Object> entityMap) {
		if (fld instanceof FieldEntity<?>) {
			FieldEntity<?> fldEntity = (FieldEntity<?>) fld;
			EntitySchema schema = PersistenceManager.getEntitySchema(fldEntity.getEntity());
			Map<String, Object> map = (Map<String, Object>) entityMap.get(fld.getId());
			return populate(map, schema);
		} else {
			return entityMap.get(fld.getId());
		}

	}

	public List<Entity> executeSelect(Select select) {
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(select.getEntityClass().getSimpleName());
		for(Restriction restriction : select.getRestrictions()){
			if (restriction.isAssociation()) {
				String[] path = restriction.getFullFieldPath();
				int pathLength = path.length;
				Criteria nestedCriteria = criteria.createCriteria(path[0]);
				for (int i = 1; i < pathLength - 1; i++) {
					nestedCriteria = nestedCriteria.createCriteria(path[i]);
				}
				Field field = findField(restriction, select.getEntitySchema());
				nestedCriteria.add(toHibernateRestriction(restriction, field.getEntitySchema()));

			} else {
				criteria = criteria.add(toHibernateRestriction(restriction, select.getEntitySchema()));
			}
		}

		List<Map<String, Object>> list = criteria.list();
		if (list == null) {
			return null;
		}

		List<Entity> result = new ArrayList<Entity>();

		EntitySchema schema = select.getEntitySchema();
		for (Map<String, Object> entityMap : list) {
			Entity entity = populate(entityMap, schema);
			result.add(entity);
		}


		return result;
	}

	
	private Criterion toHibernateRestriction(Restriction restriction, EntitySchema schema) {
		Object value = restriction.getValue();
		Field fld = schema.getField(restriction.getLeafFieldId());
		if (fld instanceof FieldEntity<?>) {
			value = toHibernateMap((Entity) value);
		}
		switch (restriction.getOperation()) {
		case EQ:
			return Restrictions.eq(restriction.getLeafFieldId(), value);
		case NE:
			return Restrictions.ne(restriction.getLeafFieldId(), value);
		case CONTAINS:
			return Restrictions.like(restriction.getLeafFieldId(), (String) value, MatchMode.ANYWHERE);
		default:
			throw new IllegalArgumentException("Not handled restriction operation: " + restriction.getOperation());
		}
	}
	
	private Field findField(Restriction restriction, EntitySchema schema) {
		String fieldId = restriction.getFldId();
		if (!restriction.isAssociation()) {
			return schema.getField(fieldId);
		}else{

			String[] path = restriction.getFullFieldPath();
			String nestedField = path[0];
			String remainingPath = fieldId.substring(nestedField.length() + 1, fieldId.length());
            FieldEntity<?> fldEntity = (FieldEntity<?>)schema.getField(nestedField);
            EntitySchema nestedSchema = PersistenceManager.getEntitySchema(fldEntity.getEntity());
            
			Restriction newRestriction = restriction.clone(remainingPath, restriction.getValue());
			return findField(newRestriction, nestedSchema);
		}
	}

}
