package com.entityui.entity.hibernate;

import java.util.ArrayList;
import java.util.List;

import com.entityui.entity.EntitySchema;

public class HibernateConfig {

	private String connectionUrl;
	private String dbUser;
	private String dbPassword;

	private List<EntitySchema> entitySchemas = new ArrayList<EntitySchema>();

	public void addEntitySchema(EntitySchema entitySchema) {
		this.entitySchemas.add(entitySchema);
	}

	public String getConnectionUrl() {
		return this.connectionUrl;
	}

	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}

	public String getDbUser() {
		return this.dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getDbPassword() {
		return this.dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public List<EntitySchema> getEntitySchemas() {
		return this.entitySchemas;
	}

}
