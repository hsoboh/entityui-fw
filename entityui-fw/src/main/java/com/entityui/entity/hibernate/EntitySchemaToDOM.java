package com.entityui.entity.hibernate;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.entityui.entity.Entity;
import com.entityui.entity.EntitySchema;
import com.entityui.entity.Field;
import com.entityui.entity.FieldDecimal;
import com.entityui.entity.FieldEntity;
import com.entityui.entity.FieldLong;
import com.entityui.entity.FieldText;

public class EntitySchemaToDOM {

	public Document toDOM(List<EntitySchema> schemas) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("hibernate-mapping");
		doc.appendChild(rootElement);

		for (EntitySchema schema : schemas) {
			Element clazz = doc.createElement("class");
			rootElement.appendChild(clazz);

			Attr entityNameAttr = doc.createAttribute("entity-name");
			entityNameAttr.setValue(getEntityName(schema.getEntityClass()));
			clazz.setAttributeNode(entityNameAttr);

			Attr tableAttr = doc.createAttribute("table");
			tableAttr.setValue(getTableName(schema.getEntityClass()));
			clazz.setAttributeNode(tableAttr);

			for (Field fld : schema.getFields()) {
				Element fldElement = null;
				if (fld.isField(Entity.FIELD_ID)) {
					fldElement = generateIdColumn(doc, fld);
				} else if (fld instanceof FieldEntity<?>) {
					fldElement = generateEntityColumn(doc, fld);
				} else {
					fldElement = generatePrimitiveColumn(doc, fld);
				}
				clazz.appendChild(fldElement);
			}

		}

		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("file.xml"));

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		transformer.transform(source, result);

		return doc;
	}

	private Element generatePrimitiveColumn(Document doc, Field fld) {

		Element prop = doc.createElement("property");

		addNameAttribute(doc, prop, fld);

		addTypeAttribute(doc, prop, fld);

		addColumnAttribute(doc, prop, fld);

		if (fld.isIndex()) {
			addIndexAttribute(doc, prop, fld);
		}

		if (fld.isUnique()) {
			addUniqueAttribute(doc, prop, fld);
		}

		addNullableAttribute(doc, prop, fld);

		if (fld instanceof FieldDecimal) {
			addDecimalAttributes(doc, prop, fld);
		}

		return prop;

	}

	private Element generateEntityColumn(Document doc, Field fld) {
		Element prop = doc.createElement("many-to-one");

		addNameAttribute(doc, prop, fld);

		addColumnAttribute(doc, prop, fld);

		addClassAttribute(doc, prop, fld);

		if (fld.isIndex()) {
			addIndexAttribute(doc, prop, fld);
		}

		if (fld.isUnique()) {
			addUniqueAttribute(doc, prop, fld);
		}

		addNullableAttribute(doc, prop, fld);

		return prop;
	}

	private Element generateIdColumn(Document doc, Field fld) {
		Element id = doc.createElement("id");

		addNameAttribute(doc, id, fld);

		addTypeAttribute(doc, id, fld);

		addColumnAttribute(doc, id, fld);

		addGeneratorElement(doc, id, fld);

		return id;

	}

	private void addNameAttribute(Document doc, Element element, Field fld) {
		Attr nameAttr = doc.createAttribute("name");
		nameAttr.setValue(fld.getId());
		element.setAttributeNode(nameAttr);
	}

	private void addTypeAttribute(Document doc, Element element, Field fld) {
		Attr typeAttr = doc.createAttribute("type");
		typeAttr.setValue(getHibernateType(fld));
		element.setAttributeNode(typeAttr);
	}

	private void addColumnAttribute(Document doc, Element element, Field fld) {
		Attr columnAttr = doc.createAttribute("column");
		columnAttr.setValue(fld.getId().toUpperCase());
		element.setAttributeNode(columnAttr);
	}

	private void addIndexAttribute(Document doc, Element element, Field fld) {
		Attr indexAttr = doc.createAttribute("index");
		indexAttr.setValue("IDX_" + fld.getId().toUpperCase());
		element.setAttributeNode(indexAttr);
	}

	private void addUniqueAttribute(Document doc, Element element, Field fld) {
		Attr uniqueAttr = doc.createAttribute("unique");
		uniqueAttr.setValue(String.valueOf(fld.isUnique()));
		element.setAttributeNode(uniqueAttr);
	}

	private void addNullableAttribute(Document doc, Element element, Field fld) {
		Attr nullableAttr = doc.createAttribute("not-null");
		nullableAttr.setValue(String.valueOf(!fld.isNullable()));
		element.setAttributeNode(nullableAttr);
	}

	private void addDecimalAttributes(Document doc, Element element, Field fld) {
		Attr precisionAttr = doc.createAttribute("precision");
		precisionAttr.setValue("" + ((FieldDecimal) fld).getPrecision());
		element.setAttributeNode(precisionAttr);

		Attr scaleAttr = doc.createAttribute("scale");
		scaleAttr.setValue("" + ((FieldDecimal) fld).getScale());
		element.setAttributeNode(scaleAttr);
	}
	
	private void addGeneratorElement(Document doc, Element element, Field fld) {
		Element generator = doc.createElement("generator");
		element.appendChild(generator);

		Attr classAttr = doc.createAttribute("class");
		classAttr.setValue("increment");
		generator.setAttributeNode(classAttr);
	}

	private void addClassAttribute(Document doc, Element element, Field fld) {
		FieldEntity<Entity> fldEntity = (FieldEntity<Entity>) fld;
		Attr classAttr = doc.createAttribute("class");
		classAttr.setValue(getEntityName(fldEntity.getEntity()));
		element.setAttributeNode(classAttr);
	}

	private String getEntityName(Class<? extends Entity> entity) {
		return entity.getSimpleName();
	}

	private String getTableName(Class<? extends Entity> entity) {
		return entity.getSimpleName().toUpperCase();
	}

	private String getHibernateType(Field fld) {
		if (fld instanceof FieldText) {
			return "string";
		}
		if (fld instanceof FieldLong) {
			return "long";
		}
		if (fld instanceof FieldDecimal) {
			return "big_decimal";
		}
		throw new IllegalArgumentException("Field type not handled " + fld.getClass());
	}

}
