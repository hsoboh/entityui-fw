package com.entityui.entity;

public class FieldEntity<E extends Entity> extends Field {

	// private EntitySchema schema;

	private Class<? extends E> entity;

	private Field lookupField;

	public FieldEntity(String id, Class<? extends E> entityClass) {
		this(id, entityClass, null);
	}

	public FieldEntity(String id, Class<? extends E> entityClass, String caption) {
		super(id, caption);
		this.entity = entityClass;
		// this.schema = schema;
	}

	@Override
	public Object wrap(Object object) {
		if (object == null) {
			return null;
		}
		if (object instanceof Long) {
			Long id = (Long) object;
			PersistenceManager manager = PersistenceManager.getInstance();
			E value = (E) manager.load(id, this.entity);
			return value;
		} else {// instanceof Entity
			E value = (E) object;
			return value;
		}
	}

	public Class<? extends E> getEntity() {
		return this.entity;
	}

	public void setEntity(Class<? extends E> entity) {
		this.entity = entity;
	}

	public Field getLookupField() {
		return this.lookupField;
	}

	public void setLookupField(String lookupField) {
		EntitySchema schema = PersistenceManager.getEntitySchema(this.entity);
		Field field = schema.getField(lookupField);

		if (field == null) {
			throw new IllegalArgumentException("Field " + lookupField + " not found in schema");
		}

		if (field instanceof FieldEntity<?>) {
			throw new IllegalArgumentException("Lookup field should be primitive");
		}
		this.lookupField = field;
	}

	// public EntitySchema getSchema() {
	// return this.schema;
	// }
	//
	// public void setSchema(EntitySchema schema) {
	// this.schema = schema;
	// }

}
