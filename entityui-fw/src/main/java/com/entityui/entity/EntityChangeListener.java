package com.entityui.entity;

public interface EntityChangeListener {

	public void entityAdded(Entity entity);

	public void entityDeleted(long id);

	public void entityUpdated(Entity entity);

}
