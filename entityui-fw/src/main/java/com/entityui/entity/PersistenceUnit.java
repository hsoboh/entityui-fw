package com.entityui.entity;


public class PersistenceUnit {

	private final Class<? extends Entity> entity;

	public PersistenceUnit(Class<? extends Entity> entity) {
		this.entity = entity;
	}

	public Class<? extends Entity> getEntity() {
		return this.entity;
	}
}
