package com.entityui.entity.validation;

import com.entityui.entity.Entity;
import com.entityui.entity.Field;

public abstract class ValidationException extends Exception {

	private static final long serialVersionUID = 1130217633030562943L;

	private Entity entity;
	private Field fld;

	public ValidationException(Entity entity, Field fld) {
		this.entity = entity;
		this.fld = fld;
	}

	public Entity getEntity() {
		return this.entity;
	}

	public Field getFld() {
		return this.fld;
	}

	public abstract String getValidationMessage();

	@Override
	public String toString() {
		return super.toString() + " " + getValidationMessage();
	}
}
