package com.entityui.entity.validation;

import com.entityui.entity.Entity;
import com.entityui.entity.Field;

public class InvalidValueException extends ValidationException {
	private static final long serialVersionUID = 409159259143309360L;

	private String message;

	public InvalidValueException(Entity entity, Field fld, String message) {
		super(entity, fld);
		this.message = message;
	}

	@Override
	public String getValidationMessage() {
		return this.message;
	}

}
