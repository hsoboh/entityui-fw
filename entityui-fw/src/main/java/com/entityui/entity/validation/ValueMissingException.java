package com.entityui.entity.validation;

import com.entityui.entity.Entity;
import com.entityui.entity.Field;

public class ValueMissingException extends ValidationException {

	private static final long serialVersionUID = 8770558975389795245L;

	public ValueMissingException(Entity entity, Field fld) {
		super(entity, fld);
	}

	@Override
	public String getValidationMessage() {
		return "Missing value for field: [" + getFld().getCaption() + "]";
	}
}
