package com.entityui.entity.validation;

import com.entityui.entity.Entity;
import com.entityui.entity.Field;

public class UniqueViolationException extends ValidationException {

	private static final long serialVersionUID = 6296326268925317519L;

	private Entity duplicateEntity;

	public UniqueViolationException(Entity entity, Field fld, Entity duplicateEntity) {
		super(entity, fld);
		this.duplicateEntity = duplicateEntity;
	}

	@Override
	public String getValidationMessage() {
		return "Duplicate value for field: [" + getFld().getCaption() + "] with existing entity: ["
				+ this.duplicateEntity.getHeadline() + "]";

	}
	

}
