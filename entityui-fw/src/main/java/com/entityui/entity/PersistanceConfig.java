package com.entityui.entity;

import java.util.ArrayList;
import java.util.List;

public class PersistanceConfig {

	private String connectionUrl;
	private String dbUser;
	private String dbPassword;

	private List<PersistenceUnit> units = new ArrayList<PersistenceUnit>();

	public void registerEntityUnit(PersistenceUnit unit) {
		if (containsUnit(unit.getEntity())) {
			throw new IllegalArgumentException("Another unit exist with the same entity = " + unit.getEntity());
		}
		this.units.add(unit);
	}

	private boolean containsUnit(Class<? extends Entity> entity) {
		for (PersistenceUnit unit : this.units) {
			if (unit.getEntity().equals(entity)) {
				return true;
			}
		}
		return false;
	}

	public List<PersistenceUnit> getUnits() {
		return this.units;
	}

	public String getConnectionUrl() {
		return this.connectionUrl;
	}

	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}

	public String getDbUser() {
		return this.dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getDbPassword() {
		return this.dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}
}
