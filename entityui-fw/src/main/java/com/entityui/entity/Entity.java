package com.entityui.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.vaadin.dialogs.ConfirmDialog;

import com.entityui.entity.validation.ValidationException;
import com.entityui.ui.EntityWindow;
import com.entityui.ui.IEntityListView;
import com.entityui.ui.IEntityView;
import com.entityui.ui.manager.EntityAction;
import com.vaadin.ui.UI;

public abstract class Entity implements Serializable {

	private static final long serialVersionUID = 6030420121117957981L;
	private static final int HASH_SEED = 7; // Should be prime
	private static final int HASH_OFFSET = 31;

	private EntitySchema schema;

	private List<FieldValuePair> fieldValues;
	
	private IEntityView view;
	private IEntityListView listView;

	private List<EntityAction> entityActions;
	private List<EntityAction> listActions;

	public static final String ACTION_SAVE = "save";
	public static final String ACTION_PRINT = "print";
	public static final String ACTION_NEW = "new";
	public static final String ACTION_OPEN = "open";
	public static final String ACTION_DELETE = "delete";
	public static final String ACTION_REFRESH = "refresh";

	public static final String FIELD_ID = "id";

	protected void buildSchema(EntitySchema schema) {
		FieldLong fld = new FieldLong(FIELD_ID);
		fld.setHidden(true);
		schema.addField(fld);
	}

	protected void setDefaults() {
		for (Field fld : getFields()) {
			setField(fld, fld.getDefaultValue());
		}
	}

	protected void init(EntitySchema schema) {
		this.schema = schema;

		this.fieldValues = new ArrayList<Entity.FieldValuePair>();
		for (Field fld : this.schema.getFields()) {
			this.fieldValues.add(new FieldValuePair(fld, null));
		}
		this.entityActions = buildEntityActions();
		this.listActions = buildListActions();

		setDefaults();
	}

	public List<EntityAction> buildEntityActions() {
		List<EntityAction> actions = new ArrayList<EntityAction>();
		actions.add(new EntityAction(ACTION_SAVE, "Save"));
		actions.add(new EntityAction(ACTION_PRINT, "Print"));
		return actions;
	}

	public List<EntityAction> buildListActions() {
		List<EntityAction> actions = new ArrayList<EntityAction>();
		actions.add(new EntityAction(ACTION_NEW, "New"));
		actions.add(new EntityAction(ACTION_OPEN, "Open"));
		actions.add(new EntityAction(ACTION_DELETE, "Delete"));
		actions.add(new EntityAction(ACTION_REFRESH, "Refresh"));
		return actions;
	}

	public void doAction(String actionId) throws ValidationException {
		EntityAction action = getAction(actionId);
		doAction(action);
	}

	public final EntityAction getAction(String actionId) {
		for (EntityAction action : this.entityActions) {
			if (action.getId().equals(actionId)) {
				return action;
			}
		}
		for (EntityAction action : this.listActions) {
			if (action.getId().equals(actionId)) {
				return action;
			}
		}
		throw new IllegalArgumentException("Not found action " + actionId);
	}

	public boolean isEnabled(EntityAction action) {
		if (action.isAction(ACTION_OPEN) || action.isAction(ACTION_DELETE)) {
			Object selected = this.listView.getSelectedEntities();
			return selected != null;
		}
		return true;
	}

	public List<EntityAction> getEntityActions() {
		return this.entityActions;
	}

	public List<EntityAction> getListActions() {
		return this.listActions;
	}

	public void doAction(EntityAction action) throws ValidationException {
		if (action.isAction(ACTION_SAVE)) {
			doSave();
		} else if (action.isAction(ACTION_PRINT)) {
			doPrint();
		} else if (action.isAction(ACTION_REFRESH)) {
			doRefresh();
		} else if (action.isAction(ACTION_NEW)) {
			doNew();
		} else if (action.isAction(ACTION_OPEN)) {
			doOpen();
		} else if (action.isAction(ACTION_DELETE)) {
			doDelete();
		} else {
			throw new IllegalStateException("Not handled action " + action.getId());
		}
	}

	private void doPrint() {
		// TODO Auto-generated method stub

	}

	private void doSave() throws ValidationException {
		save();
		this.view.closeUI();
	}

	public final void save() throws ValidationException {
		preSave();

		validate();

		PersistenceManager manager = PersistenceManager.getInstance();

		// save entity tree
		for (FieldValuePair pair : this.fieldValues) {
			Field fld = pair.getFld();
			if (fld instanceof FieldEntity<?>) {
				Entity value = getEntity(fld);
				value.save();
			}
		}
		manager.save(this);

		postSave();
	}

	private void doDelete() {
		Object selected = this.listView.getSelectedEntities();
		if (selected == null) {
			throw new IllegalStateException("No entity selected");
		}
		
		long entityId = (Long) selected;
		final PersistenceManager manager = PersistenceManager.getInstance();
		final Entity entity = manager.load(entityId, getClass());
		
		String msg = "Are you sure you want to delete " + entity.buildEntityCaption() + "?";
		ConfirmDialog.show(UI.getCurrent(), "Confirm", msg, "Delete", "Cancel", new ConfirmDialog.Listener() {

			private static final long serialVersionUID = 3286557979695250236L;

			public void onClose(ConfirmDialog dialog) {
				if (dialog.isConfirmed()) {
					manager.delete(entity);
				}
			}
		});
	}

	private void doOpen() {
		Object selected = this.listView.getSelectedEntities();
		if (selected == null) {
			throw new IllegalStateException("No entity selected");
		}
		long entityId = (Long) selected;
		Entity entity = PersistenceManager.getInstance().load(entityId, getClass());
		showEntity(entity);
	}

	private void doNew() {
		EntitySchema schema = PersistenceManager.getEntitySchema(getClass());
		Entity entity = schema.newEntity();
		showEntity(entity);
	}

	public final void showEntity(Entity entity) {
		EntityWindow window = new EntityWindow(entity);
		entity.setView(window);

		window.init();

		UI.getCurrent().addWindow(window);
	}

	private void doRefresh() {
		PersistenceManager manager = PersistenceManager.getInstance();
		List<? extends Entity> entities = manager.loadEntities(getClass());
		this.listView.setEntities(entities);
	}

	public void validate() throws ValidationException {
		for (Field fld : getFields()) {
			validate(fld);
		}
	}

	public void validate(Field fld) throws ValidationException {
		fld.validate(this);
	}

	public final void setField(Field fld, Object value) {
		Object oldValue = getField(fld);
		// trigger change only if value not same, to avoid infinite loop with
		// view
		if (!Field.equals(oldValue, value)) {
			value = fld.wrap(value);
			FieldValuePair pair = getFieldValuePair(fld);
			pair.setValue(value);
			if (this.view != null && !fld.isHidden()) {
				this.view.setField(fld.getId(), value);
			}
			fieldChanged(fld, oldValue, value);
		}
	}

	public final void setField(String fldId, Object value) {
		Field fld = field(fldId);
		if (fld == null) {
			throw new IllegalArgumentException("Field not found " + fldId);
		}
		setField(fld, value);
	}

	public final Field field(String fldId) {
		return this.schema.getField(fldId);
	}

	public final Object getField(Field fld) {
		FieldValuePair pair = getFieldValuePair(fld);
		return pair.getValue();
	}

	public final Object getField(String fldId) {
		Field fld = field(fldId);
		if (fld == null) {
			throw new IllegalArgumentException("Field not found " + fldId);
		}
		return getField(fld);
	}

	public final String getString(String fldId) {
		return (String) getField(fldId);
	}

	public final String getString(Field fld) {
		return (String) getField(fld);
	}

	public final Long getLong(String fldId) {
		return (Long) getField(fldId);
	}

	public final Long getLong(Field fld) {
		return (Long) getField(fld);
	}

	public final BigDecimal getDecimal(String fldId) {
		return (BigDecimal) getField(fldId);
	}

	public final BigDecimal getDecimal(Field fld) {
		return (BigDecimal) getField(fld);
	}

	public final Entity getEntity(String fldId) {
		return (Entity) getField(fldId);
	}

	public final Entity getEntity(Field fld) {
		return (Entity) getField(fld);
	}

	public void fieldChanged(Field fld, Object oldValue, Object newValue) {

	}

	public void preSave() {

	}

	public void postSave() {

	}

	public final EntitySchema getSchema() {
		return this.schema;
	}

	public final List<Field> getFields() {
		List<Field> fields = new ArrayList<Field>();
		for (FieldValuePair pair : this.fieldValues) {
			fields.add(pair.getFld());
		}
		return fields;
	}

	public final boolean isNull(String fldName) {
		return isNull(field(fldName));
	}

	public IEntityView getView() {
		return this.view;
	}

	public void setView(IEntityView view) {
		this.view = view;
	}

	public IEntityListView getListView() {
		return this.listView;
	}

	public void setListView(IEntityListView listView) {
		this.listView = listView;
	}

	public final boolean isNull(Field fld) {
		return fld.isNull(getField(fld));
	}

	public final boolean isNew() {
		return isNull(FIELD_ID);
	}

	public final long getId() {
		return getLong(FIELD_ID);
	}

	@Override
	public String toString() {
		String value = super.toString() + " ";
		value += " ID:" + getField(FIELD_ID);
		return value;
	}

	@Override
	public final boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (Entity.class.isAssignableFrom(obj.getClass())) {
			return !isNew() && getId() == ((Entity) obj).getId();
		}
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		if (isNew()) {
			return super.hashCode();
		}
		int intHash = HASH_SEED;

		// Sum hash codes for individual fields for final hash code
		intHash = (intHash * HASH_OFFSET) + Long.valueOf(getId()).hashCode();

		return intHash;
	}

	public List<? extends Entity> loadEntities() {
		PersistenceManager manager = PersistenceManager.getInstance();
		return manager.loadEntities(this.schema.getEntityClass());
	}

	public String getCaption() {
		return getClass().getSimpleName();
	}

	public final String buildEntityCaption() {
		if (isNew()) {
			return "Add " + getCaption();
		} else {
			return getCaption() + ": " + getHeadline();
		}
	}

	public abstract String getHeadline();

	public boolean isEnabled(Field fld) {
		return fld.isEnabled();
	}
	
	private FieldValuePair getFieldValuePair(Field fld){
		for(FieldValuePair pair: this.fieldValues){
			if (fld == pair.getFld()) {
				return pair;
			}
		}
		throw new IllegalArgumentException("Not found field value pair for field " + fld.getId());
	}

	class FieldValuePair {
		private Field fld;
		private Object value;

		public FieldValuePair(Field fld, Object value) {
			super();
			this.fld = fld;
			this.value = value;
		}

		public Object getValue() {
			return this.value;
		}

		public Field getFld() {
			return this.fld;
		}

		public void setFld(Field fld) {
			this.fld = fld;
		}

		public void setValue(Object value) {
			this.value = value;
		}
		
	}


}
