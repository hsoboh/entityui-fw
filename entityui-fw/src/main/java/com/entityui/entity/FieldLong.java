package com.entityui.entity;

public class FieldLong extends Field {

	public FieldLong(String id) {
		this(id, null);
	}

	public FieldLong(String id, String caption) {
		super(id, caption);
	}

	@Override
	public Object wrap(Object object) {
		if (object == null) {
			return null;
		}
		Long longValue = (Long) object;
		return longValue;
	}
}
